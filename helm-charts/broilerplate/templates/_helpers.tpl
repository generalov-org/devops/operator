{{/*
Expand the name of the chart.
*/}}
{{- define "broilerplate.name" -}}
{{- default .Chart.Name .Values.nameOverride | trunc 63 | trimSuffix "-" }}
{{- end }}


{{/*
Create a default fully qualified app name.
We truncate at 63 chars because some Kubernetes name fields are limited to this (by the DNS naming spec).
If release name contains chart name it will be used as a full name.
*/}}
{{- define "broilerplate.fullname" -}}
{{- if .Values.fullnameOverride }}
{{- .Values.fullnameOverride | trunc 63 | trimSuffix "-" }}
{{- else }}
{{- $name := default .Chart.Name .Values.nameOverride }}
{{- if contains $name .Release.Name }}
{{- .Release.Name | trunc 63 | trimSuffix "-" }}
{{- else }}
{{- printf "%s-%s" .Release.Name $name | trunc 63 | trimSuffix "-" }}
{{- end }}
{{- end }}
{{- end }}


{{/*
Create chart name and version as used by the chart label.
*/}}
{{- define "broilerplate.chart" -}}
{{- printf "%s-%s" .Chart.Name .Chart.Version | replace "+" "_" | trunc 63 | trimSuffix "-" }}
{{- end }}


{{/*
Common labels
*/}}
{{- define "broilerplate.labels" -}}
helm.sh/chart: {{ include "broilerplate.chart" . }}
{{ include "broilerplate.selectorLabels" . }}
{{- if .Chart.AppVersion }}
app.kubernetes.io/version: {{ .Chart.AppVersion | quote }}
{{- end }}
app.kubernetes.io/managed-by: {{ .Release.Service }}
{{- end }}


{{/*
Selector labels
*/}}
{{- define "broilerplate.selectorLabels" -}}
app.kubernetes.io/name: {{ include "broilerplate.name" . }}
app.kubernetes.io/instance: {{ .Release.Name }}
{{- end }}



{{/*
defaults
====================================================================================================================================================================================================
defaults
*/}}


{{/*
Build payload
*/}}
{{- define "payload" }}
{{- include "payload.object.kind" . }}
{{- include "payload.object.apiVersion" . }}
{{ include "payload.object.metadata" . }}
{{ include "payload.object.spec" . }}
{{- end }}

{{/*
Build service
*/}}
{{- define "service" }}
{{ include "payload.service.kind" . }}
{{ include "payload.service.apiVersion" . }}
{{ include "payload.object.metadata" . }}
{{ include "payload.service.spec" . }}
{{- end }}


{{/*
Generate object metadata
*/}}
{{- define "payload.object.metadata" -}}
metadata:
  name: {{ include "broilerplate.fullname" . }}
  labels:
    {{- include "broilerplate.labels" . | nindent 4 }}
{{- end }}


{{/*
Generate object Kind
*/}}
{{- define "payload.object.kind" -}}
{{- if .Values.payload.kind -}}
kind: {{ .Values.payload.kind }}
{{- else -}}
kind: Deployment
{{- end }}
{{- end }}


{{/*
Generate apiVersion
*/}}
{{- define "payload.object.apiVersion" -}}
{{- if .Values.payload.apiVersion }}
apiVersion: {{ .Values.payload.apiVersion }}
{{- else }}

{{- if eq .Values.payload.kind "Pod" }}
apiVersion: v1
{{- end }}

{{- if eq .Values.payload.kind "Deployment" }}
apiVersion: apps/v1
{{- end }}

{{- if eq .Values.payload.kind "StatefulSet" }}
apiVersion: apps/v1
{{- end }}

{{- if eq .Values.payload.kind "DaemonSet" }}
apiVersion: apps/v1
{{- end }}

{{- if eq .Values.payload.kind "ReplicaSet" }}
apiVersion: apps/v1
{{- end }}

{{- if eq .Values.payload.kind "Job" }}
apiVersion: batch/v1
{{- end }}

{{- if eq .Values.payload.kind "CronJob" }}
apiVersion: batch/v1beta1
{{- end }}

{{- end }}
{{- end }}


{{/*
Generate pod metadata
*/}}
{{- define "payload.pod.metadata" -}}
metadata:
  annotations:
  {{- if .Values.configuration }}
    configuration: {{ toJson .Values.configuration | sha256sum | quote }}
  {{- end }}
  {{- with .Values.payload.pod.annotations }}
    {{- toYaml . | nindent 4 }}
  {{- end }}
  labels:
    {{- include "broilerplate.selectorLabels" . | nindent 4 }}
{{- end }}


{{/*
Generate object spec
*/}}
{{- define "payload.object.spec" -}}

{{- if has .Values.payload.kind .Values.groups.first }}
{{- include "payload.pod.spec" . }}
{{- end }}

{{- if has .Values.payload.kind .Values.groups.second }}
{{- if eq .Values.payload.kind "Job" }}
{{- include "payload.job.spec" .}}
{{- else }}
{{- include "payload.cronjob.spec" .}}
{{- end }}
{{- end }}

{{- if has .Values.payload.kind .Values.groups.third }}
spec:
  {{- if eq .Values.payload.kind "StatefulSet" }}
  serviceName: {{ include "broilerplate.fullname" . }}
  {{- end }}
  
  {{- if and (not .Values.payload.autoscaling.enabled) (ne .Values.payload.kind "DaemonSet") }}
  replicas: {{ .Values.payload.replicas }}
  {{- end }}
  selector:
    matchLabels:
      {{- include "broilerplate.selectorLabels" . | nindent 6 }}
  template:
    {{- include "payload.pod.metadata" . | nindent 4 }}
    {{- include "payload.pod.spec" . | nindent 4 }}

  {{- if eq .Values.payload.kind "StatefulSet" }}
  {{- if .Values.payload.volumeClaimTemplates }}
  volumeClaimTemplates:
    {{- toYaml .Values.payload.volumeClaimTemplates | nindent 4 }}
  {{- end }}
  {{- end }}

{{- end }}

{{- end }}


{{/*
Generate job spec
*/}}
{{- define "payload.job.spec" -}}
spec:
  backoffLimit: {{ .Values.payload.job.backoffLimit }}
  template:
    {{- include "payload.pod.spec" . | nindent 4 }}
      restartPolicy: {{ .Values.payload.job.restartPolicy }}
{{- end -}}


{{/*
Generate cronjob spec
*/}}
{{- define "payload.cronjob.spec" -}}
spec:
  schedule: {{ .Values.payload.job.schedule | quote }}
  jobTemplate:
    spec:
      template:
        {{- include "payload.pod.spec" . | nindent 8 }}
{{- end -}}

{{/*
Generate pod spec
*/}}
{{- define "payload.pod.spec" -}}
spec:
  automountServiceAccountToken: false
  
  {{- with .Values.payload.terminationGracePeriodSeconds }}
  terminationGracePeriodSeconds: {{ . }}
  {{- end }}
  
  {{- with .Values.payload.hostAliases }}
  hostAliases:
  {{- toYaml . | nindent 4 }}
  {{- end }}
  
  {{- with .Values.payload.hostNetwork }}
  hostNetwork: {{ . }}
  {{- end }}
  
  {{- with .Values.payload.dnsPolicy }}
  dnsPolicy: {{ . }}
  {{- end }}
  
  {{- include "payload.init" . | nindent 2 }}
  
  {{- if or .Values.payload.image.pullSecrets .Values.payload.image.credentials }}
  imagePullSecrets:
  {{- if .Values.payload.image.credentials }}
    - name: {{ include "broilerplate.fullname" . }}
  {{- end }}
  {{- with .Values.payload.image.pullSecrets }}
    {{- toYaml . | nindent 4 }}
  {{- end }}
  {{- end }}
  
  {{- with .Values.payload.pod.securityContext }}
  securityContext:
    {{- toYaml . | nindent 4 }}
  {{- end }}
  
  containers:
    {{- include "payload.container" . | nindent 4 }}
  {{- include "payload.pod.nsat" . | nindent 2 }}
  
  {{- if or .Values.payload.volumes .Values.configuration}}
  volumes:
    - name: config
      configMap:
        name: {{ include "broilerplate.fullname" . }}
        defaultMode: 0777
        items:
        {{- range $el := .Values.configuration }}
          - key: {{ $el.name }}
            path: {{ $el.name }}
        {{- end }}
    
    {{- with .Values.payload.volumes }}
    {{- toYaml . | nindent 4 }}
    {{- end }}
  {{- end }}
{{- end }}


{{/*
Generate containers
*/}}
{{- define "payload.container" -}}
- name: {{ include "broilerplate.fullname" . }}
  {{- with .Values.payload.securityContext }}
  securityContext:
    {{- toYaml . | nindent 4 }}
  {{- end }}
  
  {{- include "payload.container.image" . | nindent 2 }}
  {{- include "payload.container.ports" . | nindent 2 }}
  {{- include "payload.container.probes" . | nindent 2 }}
  
  {{- if .Values.payload.command }}
  command:
    {{- toYaml .Values.payload.command | nindent 4 }}
  {{- end }}
  
  {{- $fullname := include "broilerplate.fullname" . }}
  {{- if .Values.payload.env }}
  env:
    {{- range $key, $val := .Values.payload.env }}
    - name: {{ $key | quote }}
      value: {{ $val | toString | replace "RELEASE_NAME" $fullname | quote }}
    {{- end }}
  {{- end }}
  
  {{- with .Values.payload.resources }}
  resources:
    {{- toYaml . | nindent 4 }}
  {{- end }}
  
  {{- if or .Values.configuration .Values.payload.volumeMounts }}
  volumeMounts:
    {{- with .Values.payload.volumeMounts }}
    {{- toYaml . | nindent 4 }}
    {{- end }}
    
    {{- range $el := .Values.configuration }}
    - name: config
      mountPath: {{ $el.mount }}
      subPath: {{ $el.name }}
      readOnly: true
    {{- end }}
    
  {{- end }}
  
{{- end }}


{{/*
Generate init container(s)
*/}}
{{- define "payload.init" -}}
{{- $fullname := include "broilerplate.fullname" . }}
{{- $image := include "payload.container.image" . }}
{{- if .Values.payload.init.enabled }}
initContainers:
  {{- range $container := .Values.payload.init.containers }}
  - name: {{ $container.name }}
    {{- if $container.image }}
    image: {{ $container.image }}
    {{- else }}
    {{ $image | nindent 4 }}
    {{- end }}
    {{- if $.Values.payload.env }}
    env:
      {{- range $key, $val := $.Values.payload.env }}
      - name: {{ $key | quote }}
        value: {{ $val | toString | replace "RELEASE_NAME" $fullname | quote }}
      {{- end }}
    {{- end }}
    {{- if $container.command }}
    command:
      {{- toYaml $container.command | nindent 6 }}
    {{- end }}
    {{- if or $.Values.configuration $.Values.payload.volumeMounts }}
    volumeMounts:
      {{- toYaml $.Values.payload.volumeMounts | nindent 6 }}
      {{- range $el := $.Values.configuration }}
      - name: config
        mountPath: {{ $el.mount }}
        subPath: {{ $el.name }}
        readOnly: true
      {{- end }}
    {{- end }}
  {{- end }}
{{- end }}
{{- end }}


{{/*
Generate container image
*/}}
{{- define "payload.container.image" -}}
image: "{{ .Values.payload.image.repository }}:{{ .Values.payload.image.tag | default .Chart.AppVersion }}"
{{- with .Values.payload.image.pullPolicy }}
imagePullPolicy: {{ . | quote }}
{{- end }}
{{- end }}


{{/*
Generate pod NSAT (nodeSelector, affinity and tolerations)
*/}}
{{- define "payload.pod.nsat" -}}
{{- with .Values.payload.nodeSelector }}
nodeSelector:
  {{- toYaml . | nindent 2 }}
{{- end }}
{{- with .Values.payload.affinity }}
affinity:
  {{- toYaml . | nindent 2 }}
{{- end }}
{{- with .Values.payload.tolerations }}
tolerations:
  {{- toYaml . | nindent 2 }}
{{- end }}
{{- end }}







{{/*
Generate object Kind
*/}}
{{- define "payload.service.kind" -}}
kind: Service
{{- end }}


{{/*
Generate apiVersion
*/}}
{{- define "payload.service.apiVersion" -}}
apiVersion: v1
{{- end }}


{{/*
Generate container ports
*/}}
{{- define "payload.container.ports" -}}
{{- if .Values.ports -}}
ports:
  {{- range $el := .Values.ports }}
  - name: {{ $el.name }}
    containerPort: {{ $el.containerPort }}
    protocol: {{ $el.protocol }}
  {{- end }}
{{- end }}
{{- end }}


{{/*
Generate container probes
*/}}
{{- define "payload.container.probes" -}}

{{- if .Values.payload.probe -}}

{{- with .Values.payload.probe.liveness }}
livenessProbe:
  {{- toYaml . | nindent 2 }}
{{- end }}

{{- with .Values.payload.probe.readiness }}
readinessProbe:
  {{- toYaml . | nindent 2 }}
{{- end }}

{{- end }}

{{- end }}


{{/*
Generate apiVersion
*/}}
{{- define "payload.service.spec" -}}
spec:
  {{- if .Values.payload.service }}
  {{- if .Values.payload.service.type }}
  type: {{ .Values.service.type }}
  {{- if eq .Values.service.type "ClusterIP" }}
  clusterIP: None
  {{- end }}
  {{- end }}
  {{- end }}
  ports:
    {{- range $el := .Values.ports }}
    - name: {{ $el.name }}
      targetPort: {{ $el.containerPort }}
      port: {{ $el.servicePort }}
      protocol: {{ $el.protocol }}
    {{- end }}
  selector:
    {{- include "broilerplate.selectorLabels" . | nindent 4 }}
{{- end }}



{{/*
Create the imagePullSecret
*/}}
{{- define "imagePullSecret" }}
{{- if .Values.payload.image.credentials }}
{{- with .Values.payload.image.credentials }}
{{- printf "{\"auths\":{\"%s\":{\"username\":\"%s\",\"password\":\"%s\",\"auth\":\"%s\"}}}" .registry .username .password (printf "%s:%s" .username .password | b64enc) | b64enc }}
{{- end }}
{{- end }}
{{- end }}
